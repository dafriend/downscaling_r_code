#this file performs the downscaling and saves out the results. To run it,
#you must have the the PRISM normals already calculated, which is done in 
#get_prism_normals.R.

start_time = Sys.time()
print("=================================================================")
print("=================================================================")
print("=================================================================")
print(paste0("Starting at ", start_time))
#===========================================
#LOAD AND PREPARE DATA
#===========================================

init_path = "/Users/dfriend/Documents/downscaling/downscaling_r_code/downscaling_code/init.R"
source(init_path)

dir.create(ds_output_dir)


#------------------
#get PRISM normals
#------------------
#I'll retrieve the normals for each variable and store them in a named list
prism_normals_var_dirs = dir(prism_clip_norm_dir, full.names = FALSE)
prism_normals = lapply(1:length(prism_normals_var_dirs), function(i){
   dir_i = prism_normals_var_dirs[i]
   files_i = dir(paste0(prism_clip_norm_dir,"/", dir_i), full.names = TRUE)
   bils_i = files_i[grepl(".bil$", files_i)]
   normals_i = raster::brick(as.list(bils_i))
   return(normals_i)
})
#prism_normals
names(prism_normals) = prism_normals_var_dirs
#raster::plot(prism_normals$tmin)

#------------------
#make a dictionary for traslating between the PRISM and CMIP variable names
#------------------
#this data frame has two purposes - it contains the variables we wish to downscale, and it also associates the PRISM and CMIP names that refer to the same variable
var_dict = data.frame(CMIP = c("pr", "tasmax", "tasmin"), PRISM = c("ppt", "tmax", "tmin"), stringsAsFactors = FALSE)

#------------------
#get the polygon that tells us the geographical extent we're interested in
#------------------
proj_string = "+init=epsg:4326"
box_sp = rgdal::readOGR(boundary_shp)
box_ll = sp::spTransform(box_sp, sp::CRS(proj_string)) #ll is for lat-lon
box_ll_ext = raster::extent(box_ll)


#------------------
#get the folder paths for the various model runs that contain the CMIP projections
#------------------
#box_folders_full = dir(cmip5_dir, full.names = TRUE)
#box_folders = dir(cmip5_dir)

#get the ".bil" files we want
all_files = dir(cmip5_dir, recursive = TRUE, full.names = TRUE)
ncdf_files_bool = grepl(paste0(var_dict$CMIP, ".nc$", collapse = "|"), all_files) #create a boolean that we'll use to identify the files that contain the variables we want
ncdf_files = all_files[ncdf_files_bool]

#create an output directory for this "box"
# box_output_dir_i = paste0(ds_output_dir, "/", box_folders[i])
# dir.create(box_output_dir_i)

#===========================================
#
#===========================================

#################### !!!!!!!!!!!! NOTE !!!!!!!!!  vvvvvvvvvvvvvvvvvvvvvvvvv
#This next line is a bit of a hack - I want to use the CMIP data that's in the 'bcsd5' subdirectory for 
#each RCP and box, so I'll filter it down to only those files. There's another directory in each one
#called '1_8obs' that's screwing things up. Those NCDF files have more than one variable, and my function
#is only built to deal with one variable.
ncdf_files = ncdf_files[grepl("/bcsd5/", ncdf_files)]

#loop over each of the "box" folders, each of which contains one CMIP run for each variable
#parallel::mclapply(1:length(ncdf_files), function(i){
lapply(1:length(ncdf_files), function(i){
   loop_stime = Sys.time()
   
   print("=================================================================")
   print(paste0("Started ", ncdf_files[i]))
   print(loop_stime)
   #------------------
   #create the output directories
   #------------------
   
   #create a folder for this "box" (if it doesn't already exist)
   path_split = strsplit(ncdf_files[i], "/")[[1]]
   box_i = path_split[grepl("rcp\\d{2}_box\\d", path_split)]
   output_dir_i = paste0(ds_output_dir, "/", box_i)
   if(!dir.exists(output_dir_i)){
      dir.create(output_dir_i)
   }
   
   #extract the variable name
   file_name_i = path_split[length(path_split)]
   reg_ex_i = regexpr("_\\w+.",file_name_i)
   cmip_var_name_i = substr(file_name_i, reg_ex_i+1, reg_ex_i + (attr(reg_ex_i, "match.length")-2))
   prism_var_name_i = var_dict$PRISM[var_dict$CMIP == cmip_var_name_i]
   
   #create a folder for this variable
   var_dir_i = paste0(output_dir_i, "/", cmip_var_name_i)
   if(!dir.exists(var_dir_i)){
      dir.create(var_dir_i)
   }
   
   #within the folder for the variable, create one folder for each year
   sapply(new_start_year:new_end_year, function(year_i){
      year_output_dir_i = paste0(var_dir_i, "/", year_i)
      if(!dir.exists(year_output_dir_i)){
         dir.create(year_output_dir_i)
      }
      return(NA)
   })
   
   #------------------
   #retrieve the CMIP data 
   #------------------
   #check if this variable is precip (pr) - we need to know this since precipitation is treated slightly differently than other variables
   is_precip = FALSE
   if(grepl("pr.nc$", ncdf_files[i])){
      is_precip = TRUE
   }
   
   #get the data for the first chunk of years
   model_past_list = ncdf_extract_data(ncdf_file_name = ncdf_files[i],
                                  start_year = old_start_year,
                                  start_month = old_start_month,
                                  end_year = old_end_year,
                                  end_month = old_end_month,
                                  xmin = box_ll_ext[1], 
                                  xmax = box_ll_ext[2], 
                                  ymin = box_ll_ext[3], 
                                  ymax = box_ll_ext[4])
   model_past = model_past_list$data
   model_past_dates = model_past_list$dates
   
   #get the data for the second chunk of years
   model_future_list = ncdf_extract_data(ncdf_files[i],
                                    start_year = new_start_year,
                                    start_month = new_start_month,
                                    end_year = new_end_year,
                                    end_month = new_end_month,
                                    xmin = box_ll_ext[1], 
                                    xmax = box_ll_ext[2], 
                                    ymin = box_ll_ext[3], 
                                    ymax = box_ll_ext[4])
   model_future = model_future_list$data
   model_future_dates = model_future_list$dates
   
   #if this is precipitation data, we need to convert the units (CMIP puts it in mm/day, and we want it in mm/month (which is how PRISM does it))
   if(is_precip){
      model_past = model_past*lubridate::days_in_month(model_past_dates)
      model_future = model_future*lubridate::days_in_month(model_future_dates)
   }
   
   #------------------
   #perform downscaling
   #------------------
   model_clim = get_climatology(model_past, n_cores = 1)
   
   anom_method = "subtraction"
   if(is_precip){
     anom_method = "division"
   }
   

   model_anomalies = get_anomalies(model_clim, model_future, method = anom_method) #get the anomalies
   ds_list = parallel::mclapply(1:raster::nlayers(model_anomalies), function(j){
      #model_anomalies_ds = downscale(model_anomalies[[j]], prism_normals[[prism_var_name_i]]) #downscale the anomalies 
      model_anomalies_ds = raster::resample(model_anomalies[[j]], prism_normals[[prism_var_name_i]], method = "bilinear")
      month_n = j %% 12
      if(month_n == 0){
         month_n = 12
      }
      if(is_precip){
         model_future_ds = prism_normals[[prism_var_name_i]][[month_n]] * model_anomalies_ds
      } else {
         model_future_ds = prism_normals[[prism_var_name_i]][[month_n]] + model_anomalies_ds
      }
      names(model_future_ds) = names(model_future)[j]
      raster::crs(model_future_ds) = raster::crs(prism_normals)
      file_path_j = paste0(var_dir_i, "/", lubridate::year(model_future_dates[j]), "/", box_i, "_", names(model_future_ds), ".bil")
      raster::writeRaster(model_future_ds, file_path_j, overwrite=TRUE)
   }, mc.cores = 12)
   
   loop_etime = Sys.time()
   loop_diftime = loop_etime - loop_stime
   units(loop_diftime) = "mins"
   
   print(paste0("Finished ", ncdf_files[i]))
   print(loop_etime)
   print("------------")
   print(paste0("Total time: ", loop_diftime, " mins"))
   print("=================================================================")
})
#}, mc.cores = 12)

end_time = Sys.time()
time_dif = end_time - start_time
units(time_dif) = "hours"
print(paste0("Finished at ", end_time))
print(paste0("Total time was ", round(time_dif, 2), " hours"))

print("=================================================================")
print("=================================================================")
print("=================================================================")


