This repository contains code used for downscaling future climate projections using historical PRISM data.

### File structure

* downscaling_code - Contains the R scripts used for the actual downscaling. More info on this below.
* make_plots - contains R scripts used for making plots
* scratch - the catch-all folder for anything that doesn't fall into the previous two categories. As the name suggests,
most of these files are just scratchwork, and the *real* downscaling code is all in 'downscaling_code'
* trash - scripts that I no longer use but am too scared to delete just in case they might be useful someday


### The downscaling_code directory

This directory contains the core code for performing downscaling. See 'The downscaling process' below for more on these files.

#### The 'fx' folder

This folder contains scripts that define functions used in downscaling.


* downscale_fx.R - contains the actual code for performing the downscaling.
* ncdf_extract_data_fx.R - contains code for extracting data from a NetCDF file. 
* prism_get_data_fx.R - contains code for easily reading in the PRISM data. 
* cmip_ds_get_data_fx.R - contains functions for easily reading in the data that has been downscaled. Like 
prism_get_data_fx.R, these functions are dependent on the naming scheme.

### Understanding the files in this repository

It's important to make some distinctions about the purposes of various scripts. The core downscaling code (in
'downscaling_code/fx/downscale_fx.R') is actually fairly simple, and there's not a lot to it. A large chunk of the 
code is dedicated to performing the downscaling process in bulk. 

Most of the code is dedicated to things like reading in the data, cropping the data, and performing the downscaling in bulk. 



### The downscaling process

All of the parameter input (i.e. file paths, years to use, etc.) occurs in 'downscaling_code/init.R'. This is the ONLY place that you'll need to modify file paths and other parameter. This file also automatically sources the files in 'fx' so that they are in memory. Each of the files discussed below begins by sourcing 'init.R'. 

1. Run buffer_boundary.R, which buffers the polygon used as the boundary. This can be skipped if you don't need to buffer the polygon, or the polygon is pre-buffered. This code buffers the polygon four cell sizes out in each direction and then saves the polygon out to a shapefile.

2. Run clip_prism_data.R. It's faster to clip out the area we want from the PRISM data beforehand, rather than loading in the entire raster during the downscaling process. Because of this, I use this script to clip the PRISM climate data to the polygon we made in 'buffer_boundary.R', and then save those files out.

3. Run get_prism_normals.R. This script takes the clipped PRISM data just created and calculates 30 year normals, and then saves the data.

4. Run downscale_batch.R. This is where the downscaling is actually performed. 

### Some notes

An unfortunate aspect of this code is that it is that in order to perform the downscaling in 'batch' mode, the folder structure and file names must all follow the conventions I've used so far, and all the data must be in the same format. In particular, the current code assumes that the data to be downscaled is in NCDF format, and it assumes a very specific file structure and naming convention for the PRISM data. In addition, it also makes assumptions about variable names - so if we try to downscale a different data set, we'll probably need to change some of the climate variable names.

Fortunately, though, it seems likely that the PRISM data will always be used as the reference data - we're more likely to change the data source we want to perform downscaling on. If that is the case, we might need to change some things in 'downscaling_batch.R' - but I *think* the other files can remain unchanged so long as we're still using the PRISM data.
