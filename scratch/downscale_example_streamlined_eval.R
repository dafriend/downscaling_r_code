#this script relies on the code in 'downscale_example_streamlined.R' - it assumes that that script has been run.
#this script is meant to do more in depth analysis of the results, in order to see if it's actually doing what it's supposed to do.

#let's look at a single raster
compare_indices = c(1:raster::nlayers(model_future))
pre = model_future[[compare_indices]]
post = model_future_ds[[compare_indices]]

#let's look at the max and min of the rasters
pre_max = raster::maxValue(pre)
pre_min = raster::minValue(pre)

post_max = raster::maxValue(post)
post_min = raster::minValue(post)

#if it's doing interpolation, the values of the downscaled raster should be LESS extreme than the values of the original raster
cbind(pre_max, post_max)
pre_max > post_max

cbind(pre_min, post_min)
pre_min < post_min

#we've checked the max and min... let's check the quantiles to get a more complete picture
pre_quants = raster::quantile(pre)
post_quants = raster::quantile(post)

comb_quants = rbind(pre_quants, post_quants)
range(comb_quants)
plot(1:nrow(pre_quants), 1:nrow(pre_quants), ylim = range(comb_quants), type="n")
for(i in 1:ncol(pre_quants)){
   lines(1:nrow(pre_quants), pre_quants[,i], col="blue")
   lines(1:nrow(post_quants), post_quants[,i], col="red")
}
legend(x = "topleft", legend = c("Pre", "Post"), lty = c(1,1), col = c("blue", "red"))

#plot a histogram of the values
raster::hist(pre, breaks = seq(-10, 50, by=1))
raster::hist(post, breaks = seq(-10, 50, by=1))

par(mfrow=c(1,2))
raster::plot(pre)
raster::plot(post)


#I'm going to try and see if I can plot where the values are most different
pre_resample = raster::resample(pre, post, method = "ngb")
pre_resample_bl = raster::resample(pre, post, method = "bilinear")
resamp_dif = pre_resample_bl - post
resamp_dif2 = post - pre_resample_bl

plot_min = min(raster::minValue(resamp_dif))
plot_max = max(raster::maxValue(resamp_dif))

plot_min2 = min(raster::minValue(resamp_dif2))
plot_max2 = max(raster::maxValue(resamp_dif2))

plot_breaks = seq(plot_min, plot_max, length.out = 100)
plot_breaks2 = seq(plot_min2, plot_max2, length.out = 100)
pal <- colorRampPalette(c("blue", "lightblue", "white","salmon","red"))

raster::plot(resamp_dif[[1]], col = pal(length(plot_breaks)))
raster::plot(resamp_dif2[[1]], col = pal(length(plot_breaks2)))

#Actually I don't think that's the right approach... instead I'll try coloring the ones with more extreme vals a different color
i = 8
par(mfrow = c(3,4))
for(i in 1:12){
   plot_pre_min = raster::minValue(pre[[i]])
   plot_pre_max = raster::maxValue(pre[[i]])
   plot_post_min = raster::minValue(post[[i]])
   plot_post_max = raster::maxValue(post[[i]])
   plot_min_i = min(c(plot_pre_min, plot_post_min))
   plot_max_i = max(c(plot_pre_max, plot_post_max))
   pal2 = colorRampPalette(c("blue", "red"))
   #raster::plot(post[[i]], col=pal2(4), breaks = c(plot_pre_min-20, plot_pre_min, plot_pre_max, plot_pre_max + 20))
   plot_breaks = unique(c(plot_min_i, plot_pre_min, plot_pre_max, plot_max_i))
   if(length(plot_breaks) == 2){
      cols = "gray"
   } else if(length(plot_breaks) == 3){
      if(plot_pre_min == plot_min_i){
         cols = c("gray", "red")
      } else {
         cols = c("blue", "gray")
      }
   } else if(length(plot_breaks == 4)){
      cols = c("blue", "gray", "red")
   }
   raster::plot(post[[i]], col=cols, breaks = plot_breaks, main = paste("month ", i))
}

#I now realize that what I said before isn't true - in this case it's acceptable for the downscaled
#results to have more extreme values than the original results - that's because the *anomalies* 
#are downscaled with bilinear interpretation, and then those are added to the PRISM data. What
#I said should be true of the downscaled anomalies - but because we're adding these to the PRISM
#data this won't necessarily hold for the final result.

anom_pre = model_anomalies[[compare_indices]]
anom_post = model_anomalies_ds[[compare_indices]]

#let's look at the max and min of the rasters
anom_pre_max = raster::maxValue(anom_pre)
anom_pre_min = raster::minValue(anom_pre)

anom_post_max = raster::maxValue(anom_post)
anom_post_min = raster::minValue(anom_post)

#if it's doing interpolation, the values of the downscaled raster should be LESS extreme than the values of the original raster
cbind(anom_pre_max, anom_post_max)
anom_pre_max >= anom_post_max

cbind(anom_pre_min, anom_post_min)
anom_pre_min < anom_post_min

which(anom_pre_max <= anom_post_max & anom_pre_min >= anom_post_min)


anom_pre_quants = raster::quantile(anom_pre)
anom_post_quants = raster::quantile(anom_post)

comb_quants = rbind(anom_pre_quants, anom_post_quants)
range(comb_quants)
plot(1:nrow(anom_pre_quants), 1:nrow(anom_pre_quants), ylim = range(comb_quants), type="n")
for(i in 1:ncol(anom_pre_quants)){
   lines(1:nrow(anom_pre_quants), anom_pre_quants[,i], col="blue")
   lines(1:nrow(anom_post_quants), anom_post_quants[,i], col="red")
}
legend(x = "topleft", legend = c("anom_pre", "anom_post"), lty = c(1,1), col = c("blue", "red"))

#plot a histogram of the values
raster::hist(anom_pre, breaks = seq(-10, 50, by=1))
raster::hist(anom_post, breaks = seq(-10, 50, by=1))

par(mfrow=c(1,2))
raster::plot(anom_pre)
raster::plot(anom_post)

par(mfrow = c(3,4), bg = "lightgray")
for(i in 1:12){
   plot_anom_pre_min = raster::minValue(anom_pre[[i]])
   plot_anom_pre_max = raster::maxValue(anom_pre[[i]])
   plot_anom_post_min = raster::minValue(anom_post[[i]])
   plot_anom_post_max = raster::maxValue(anom_post[[i]])
   plot_min_i = min(c(plot_anom_pre_min, plot_anom_post_min))
   plot_max_i = max(c(plot_anom_pre_max, plot_anom_post_max))
   #pal2 = colorRampPalette(c("blue", "red"))
   #raster::plot(anom_post[[i]], col=pal2(4), breaks = c(plot_anom_pre_min-20, plot_anom_pre_min, plot_anom_pre_max, plot_anom_pre_max + 20))
   plot_breaks = unique(c(plot_min_i, plot_anom_pre_min, plot_anom_pre_max, plot_max_i))
   if(length(plot_breaks) == 2){
      cols = "white"
   } else if(length(plot_breaks) == 3){
      if(plot_anom_pre_min == plot_min_i){
         cols = c("white", "red")
      } else {
         cols = c("blue", "white")
      }
   } else if(length(plot_breaks == 4)){
      cols = c("blue", "white", "red")
   }
   if(length(plot_breaks) > 2){
      plot_title = paste0("month ", i, " | DIF")
   } else {
      plot_title = paste0("month ", i)
   }
   raster::plot(anom_post[[i]], col=cols, breaks = plot_breaks, main = plot_title)
}

#Looks like the parts where there are more extreme values is only on the edges.

#==============================================
i=12
anom_pre_i = anom_pre[[i]]
anom_post_i = anom_post[[i]]

raster::aggregate(anom_pre_i, anom_post_i, "mean")








